#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_type.h"
#include "iparse.h"
#include "jparse.h"
#include "rparse.h"
#include "typeparse.h"

/*****************************
* REFERENCE
* http://web.cse.ohio-state.edu/~crawfis.3/cse675-02/Slides/MIPS%20Instruction%20Set.pdf
*****************************/

/**
* Register name look up table
* Each index is also the matching hex code for the register
*/
const char *regName[32] = {"0", "at", "v0", "v1", "a0", "a1", "a2", "a3",
                           "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
                           "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7",
                           "t8", "t9", "k0", "k1", "gp", "sp", "s8", "ra"};

/**
* Helper function declarations
*/
void print_instr(instr_t *instrpt);
void print_binary(uint32_t hexcode);
instr_t *get_instrText(void);
void print_instr(instr_t *instrpt);
void print_jumpTB(void);

/**
* Application entry
*/
int main() {
    FILE *outFilept;
    instr_t *instrpt, *first_instrpt, *instrpt2;
    first_instrpt = get_instrText();
    if (first_instrpt == NULL) {
        return 0;
    }
    /*
    * Go through each instr object in linked list
    * to make the appropriate parsing
    */
    instrpt = first_instrpt;
    while (instrpt != NULL) {
        type_parse(instrpt);
        switch (instrpt->type) {
        case 'i':
            i_type_parse(instrpt);
            break;
        case 'j':
            j_type_parse(instrpt);
            break;
        case 'r':
            r_type_parse(instrpt);
            break;
        default:
            break;
        }
        print_instr(instrpt);
        instrpt = instrpt->next;
    }
    print_jumpTB();

    /*
    * Find dependencies of instructions
    */
    printf("\n++++++++Dependencies++++++++\n");
    instrpt = first_instrpt->next;
    while (instrpt != NULL) {
        instrpt2 = first_instrpt;
        while (instrpt2 != instrpt) {
            if (instrpt2->rd[0] != '\0' && (!strcmp(instrpt->rs, instrpt2->rd) || !strcmp(instrpt->rt, instrpt2->rd))) {
                printf("%s dependent on%s due to: %s\n", instrpt->instrString, instrpt2->instrString, instrpt2->rd);
            }
            instrpt2 = instrpt2->next;
        }
        instrpt = instrpt->next;
    }
    printf("\n++++++++End Dependencies++++++++\n");

    /*
    * Go through each instr object in linked list
    * and write the hex code to binary file
    */
    outFilept = fopen("Output.o", "w");  // write file in binary mode
    instrpt = first_instrpt;
    while (instrpt != NULL) {
        fwrite(&instrpt->hexcode, sizeof(uint32_t), 1, outFilept);
        instrpt = instrpt->next;
    }
    fclose(outFilept);

    /*
    * Free memory from linked list
    */
    instrpt = first_instrpt;
    while (instrpt != NULL) {
        instrpt = instrpt->next;
        free(first_instrpt);
        first_instrpt = instrpt;
    }

    return 0;
}


/**
* Helper function
* print an instruction object to screen
*/
void print_instr(instr_t *instrpt) {
    printf("%-10s %#010x\n", "Address:", instrpt->addr);
    printf("%-10s %s\n", "String:", instrpt->instrString);
    printf("%-10s %s\n", "Name:", instrpt->name);
    printf("%-10s %c_type\n", "Type:", instrpt->type);
    printf("%-10s %s\n", "Rd:", instrpt->rd);
    printf("%-10s %s\n", "Rs:", instrpt->rs);
    printf("%-10s %s\n", "Rt:", instrpt->rt);
    printf("%-10s %d\n", "Shamt:", instrpt->shamnt);
    printf("%-10s %d\n", "Imm:", instrpt->immed);
    printf("%-10s %s\n", "Jump to:", instrpt->jump_label);
    printf("%-10s", "Binary:");
    print_binary(instrpt->hexcode);
    printf("\n===========================================\n");
}

/**
* Helper function
* Print a 32bit instruction code in binary format to screen
*/
void print_binary(uint32_t hexcode) {
    for (int i = 31; i >= 0; i--) {
        printf("%d", (hexcode >> i) & 0x01);
        if (i == 26 || i == 21 || i == 16 || i == 11 || i == 6) {
            printf(".");
        }
    }
    printf("\n");
}

/**
* Helper function
* Print table of jump destination address
*/
void print_jumpTB(void) {
    printf("\n++++++++JUMP TABLE++++++++\n");
    for (int i = 0; i < 10; i++) {
        if(jump_table[i].label[0] == '\0') break;
        printf("%-10s", jump_table[i].label);
        printf("%#010x\n", jump_table[i].addr);
    }
    printf("\n++++++END JUMP TABLE++++++\n");
}

/**
* Helper function
* Read text file line by line and put string in instr object
* Each line create a new instr object in linked list
* calloc() is used to zero-initialize objects
*/
instr_t *get_instrText(void) {
    char tmpStr[INSLENGTH] = {0};
    FILE *inFilept;
    instr_t *instrpt, dummyHead = {0};
    int i = 0;
    uint32_t addr = 0x1000;

    inFilept = fopen("Input.txt", "r");
    if (inFilept ==  NULL) {
       printf("File not found. \n");
       return NULL;
    }

    instrpt = &dummyHead;
    while (fgets(tmpStr, INSLENGTH, inFilept) != NULL) {
        if (tmpStr[strlen(tmpStr)-1] == '\n') {
            tmpStr[strlen(tmpStr)-1] = '\0';    // discard newline character if present
        }
        if (tmpStr[strlen(tmpStr)-1] == ':') {  // if the current line is a label
            tmpStr[strlen(tmpStr)-1] = '\0';
            strcpy(jump_table[i].label, tmpStr);
            jump_table[i].addr = addr;
            i++;
            if (i >= MAXLABEL) printf("Critical Error - jump_dest buffer overflow\n");
        } else {                                // line is an instruction
            instrpt->next = (instr_t *)calloc(1, sizeof(instr_t));
            if (instrpt->next == NULL) {
                printf("Error allocating memory. \n");
                return NULL;
            }
            instrpt = instrpt->next;
            strcpy(instrpt->instrString, tmpStr);
            instrpt->addr = addr;
            addr += 4;
        }
    }
    fclose(inFilept);
    return dummyHead.next;
}
