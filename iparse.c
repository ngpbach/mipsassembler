#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "instr_type.h"
#include "iparse.h"

/**
* List of instructions that follow the format
* "name $rt $rs imm"
*/
const char *i_type1[] = {"addi", "slti", "andi"}; //TODO add more

/**
* List of instructions that follow the format
* "name $rs $rt imm"
*/
const char *i_type2[] = {"beq", "bne", NULL}; //added null so size of 3 array the same

/**
* List of instructions that follow the format
* "name $rt imm($rs)"
*/
const char *i_type3[] = {"lw", "sw", NULL}; //added null so size of 3 array the same

/**
* Helper function
* To determine what format of i-type instruction to parse accordingly
*/
int _lookup_type(char *name) {
    for (int i = 0; i < sizeof(i_type1)/sizeof(char *); i++) {
        if (!strcmp(name, i_type1[i])) return 1;
        if (!strcmp(name, i_type2[i])) return 2;
        if (!strcmp(name, i_type3[i])) return 3;
    }
    return 0;
}

/**
* Parse instruction string in the known format
* "name $rt $rs imm" or
* "name $rs $rt imm" or
* "name $rt imm($rs)"
* Put the register names, immed value and hexcode in the appropriate field
*/
void i_type_parse(instr_t *instrpt) {
    int i_type = _lookup_type(instrpt->name);
    switch(i_type) {    // get register names and immed value from string
    case 1:             // "name $rt $rs imm"
        sscanf(instrpt->instrString, "%*s $%s $%s %d", instrpt->rt, instrpt->rs, &instrpt->immed);
        break;
    case 2:             // "name $rs $rt imm"
        sscanf(instrpt->instrString, "%*s $%s $%s %d", instrpt->rs, instrpt->rt, &instrpt->immed);
        break;
    case 3:             //"name $rt imm($rs)"
        sscanf(instrpt->instrString, "%*s $%s %d($%[^)]", instrpt->rt, &instrpt->immed, instrpt->rs);
        break;
    default:
        printf("Error - cannot identify i type instruction\n");
        break;
    }

    if (instrpt->immed >= (1 << 16)) {
        printf("Immediate value exceed 16bit size\n");
        return;
    }

    instrpt->hexcode |= instrpt->immed << 0;  // set imm bits

    int count = 0;
    for (int i = 0; i < 32; i++) {
        if (!strcmpi(instrpt->rt, regName[i])) {
            instrpt->hexcode |= i << 16;      // set rt bits
            count++;
        }
        if (!strcmpi(instrpt->rs, regName[i])) {
            instrpt->hexcode |= i << 21;       // set rs bits
            count++;
        }
    }
    if (count < 2) printf("One or more register not found!\n");
}
