#ifndef RPARSE_H_INCLUDED
#define RPARSE_H_INCLUDED

#include "instr_type.h"

/**
* Parse instruction string in the known format "operation $reg1 $reg2 $reg3"
* Put the register names and hexcode in the appropriate field
*/
void r_type_parse(instr_t *instrpt);

#endif // RPARSE_H_INCLUDED
