#ifndef JPARSE_H_INCLUDED
#define JPARSE_H_INCLUDED

#include "instr_type.h"

/**
* Parse instruction string in the known format "operation jump_label"
* Relative address is 16 bit (discard 2 LSBs and 6 MSBs)
* Destination address must have 6 MSBs the same as current instruction address
*/
void j_type_parse(instr_t *instrpt);

#endif // JPARSE_H_INCLUDED
