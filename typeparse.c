#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_type.h"
#include "typeparse.h"

/**
* Instruction name look up table
*/
struct instr_LUT {
    char name[NAMELENGTH];
    char type;
    uint32_t hexcode;       // Can be either Opcode or Func, type will decide which one it would be
} instr_LUT[] = {
    {"add",     'r',    0b100000},
    {"addi",    'i',    0b001000},
    {"and",     'r',    0b100100},
    {"andi",    'i',    0b001100},
    {"or",      'r',    0b100101},
    {"j",       'j',    0b000010},
    {"sub",     'r',    0b100010},
    {"lw",      'i',    0b100011},
};
size_t instr_LUT_length = sizeof(instr_LUT)/sizeof(struct instr_LUT);

/**
*   Parse the name of the instruction
*   Fill the name and type and hexcode field appropriately
*/
void type_parse(instr_t *instrpt) {
    sscanf(instrpt->instrString, "%s", instrpt->name);     // Get instruction name from string
    /**
    * Look through the lookup table
    * TODO better to use binary search if LUT is long
    */
    for (int i = 0; i < instr_LUT_length; i++) {
        if (!strcmpi(instrpt->name, instr_LUT[i].name)) {
            instrpt->type = instr_LUT[i].type;
            if (instrpt->type == 'r') {
                instrpt->hexcode |= instr_LUT[i].hexcode << 0;   // set func bits
            } else {           // i or j type
                instrpt->hexcode |= instr_LUT[i].hexcode << 26;  // set op bits
            }
            return;
        }
    }
    printf("Instruction |%s| not found!\n", instrpt->name);
}
