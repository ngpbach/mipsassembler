#ifndef IPARSE_H_INCLUDED
#define IPARSE_H_INCLUDED

#include "instr_type.h"

/**
* Parse instruction string in the known format
* "name $rt $rs imm" or
* "name $rs $rt imm" or
* "name $rt imm($rs)"
* Put the register names, immed value and hexcode in the appropriate field
*/
void i_type_parse(instr_t *instrpt);

#endif // IPARSE_H_INCLUDED
