#ifndef INSTR_TYPE_H_INCLUDED
#define INSTR_TYPE_H_INCLUDED

#include <stdint.h>

#define INSLENGTH   30
#define NAMELENGTH  10
#define MAXLABEL    10

extern const char *regName[32];

/**
* Instruction object
* Contains parameters for 1 single instruction
* To be used with malloc and linked-list data structure.
*/
typedef struct instr_t instr_t;
struct instr_t{
    char instrString[INSLENGTH];
    char name[NAMELENGTH];
    char type;
    char rd[5];
    char rs[5];
    char rt[5];
    int immed;
    int shamnt;
    uint32_t addr;
    char jump_label[10];
    uint32_t hexcode;
    instr_t *next;
};

/**
* Array for storing and looking instruction address for the matching label
* TODO use linked list to not have a limit on the max number of jump destination
*/
struct jump_table{
    char label[20];
    uint32_t addr;
} jump_table[MAXLABEL];

#endif // INSTR_TYPE_H_INCLUDED
