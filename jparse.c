#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_type.h"
#include "jparse.h"


/**
* Parse instruction string in the known format "operation jump_label"
* Relative address is 16 bit (discard 2 LSBs then zero out 6 MSBs)
* Destination address must have 6 MSBs the same as current instruction address
* Put relative address in the appropriate field in hexcode
*/
void j_type_parse(instr_t *instrpt) {
    uint32_t relative_addr;
    sscanf(instrpt->instrString, "%*s %s", instrpt->jump_label);    // get jump to label from string
    for (int i = 0; i < MAXLABEL; i++) {
        if (!strcmp(instrpt->jump_label, jump_table[i].label)) {
            if (jump_table[i].addr >> 28 != instrpt->addr >> 28) {  // current instruction and target instruction must have same 4 MSBs
                printf("Jump destination address out of range!");
                return;
            }
            relative_addr = (jump_table[i].addr << 4) >> 6;         // discard 4 MSBs and 2 LSBs
            instrpt->hexcode |= relative_addr;                      // set the jump relative address (bits 0-25)
            return;
        }
    }
    printf("Jump destination |%s| not found!\n", instrpt->jump_label);
}
