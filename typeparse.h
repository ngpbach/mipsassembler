#ifndef TYPEPARSE_H_INCLUDED
#define TYPEPARSE_H_INCLUDED

#include "instr_type.h"

/**
*   Parse the name of the instruction
*   Fill the name and type and hexcode field appropriately
*/
void type_parse(instr_t *instrpt);

#endif // TYPEPARSE_H_INCLUDED
