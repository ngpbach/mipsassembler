#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_type.h"
#include "rparse.h"

/**
* Parse instruction string in the known format "operation $reg1 $reg2 $reg3"
* Put the register names and hexcode in the appropriate field
*/
void r_type_parse(instr_t *instrpt) {
    sscanf(instrpt->instrString, "%*s $%s $%s $%s", instrpt->rd, instrpt->rs, instrpt->rt); // get register names from string
    int count = 0;
    for (int i = 0; i < 32; i++) {
        if (!strcmpi(instrpt->rd, regName[i])) {
            instrpt->hexcode |= i << 11;      // set rd bits
            count++;
        }
        if (!strcmpi(instrpt->rt, regName[i])) {
            instrpt->hexcode |= i << 16;      // set rt bits
            count++;
        }
        if (!strcmpi(instrpt->rs, regName[i])) {
            instrpt->hexcode |= i << 21;       // set rs bits
            count++;
        }
    }
    if (count < 3) printf("One or more register not found!\n");
}
